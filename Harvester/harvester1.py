import tkinter as tk
from tkinter import ttk
from scapy.all import ARP, Ether, srp
import socket
import threading
import json
from git import Repo
import shutil

class NetworkScannerApp:
    def __init__(self, master):
        self.master = master
        master.title("Network Scanner")

        # Entrée pour la plage d'IP
        self.ip_range_label = tk.Label(master, text="Enter IP Range:")
        self.ip_range_label.pack()

        self.ip_range_entry = tk.Entry(master)
        self.ip_range_entry.pack()

        # Bouton de numérisation
        self.scan_button = tk.Button(master, text="Scan", command=self.scan_devices_on_network)
        self.scan_button.pack()

        # Zone de texte pour afficher les résultats
        self.result_text = tk.Text(master, height=10, width=40)
        self.result_text.pack()

        # Étiquette pour afficher le message de confirmation
        self.confirmation_label = tk.Label(master, text="")
        self.confirmation_label.pack()

    def get_hostname(self, ip_address):
        try:
            hostname, _, _ = socket.gethostbyaddr(ip_address)
            return hostname
        except (socket.herror, socket.gaierror, socket.timeout):
            return "N/A"

    def scan_devices_on_network(self):
        # Efface les résultats précédents
        self.result_text.delete(1.0, tk.END)

        # Obtient la plage d'IP depuis l'entrée utilisateur
        ip_range = self.ip_range_entry.get()

        # Crée une requête ARP
        arp = ARP(pdst=ip_range)
        ether = Ether(dst="ff:ff:ff:ff:ff:ff")
        packet = ether/arp

        # Lance la numérisation dans un thread pour ne pas bloquer l'interface utilisateur
        threading.Thread(target=self.scan_thread, args=(packet,), daemon=True).start()

    def scan_thread(self, packet):
        # Envoie la requête et reçoit la réponse
        result = srp(packet, timeout=3, verbose=0)[0]

        # Rassemble les résultats dans une liste de dictionnaires
        scan_results = []
        for sent, received in result:
            ip_address = received.psrc
            hostname = self.get_hostname(ip_address)
            if hostname != "N/A":
                device_info = {"Device": hostname, "IP": ip_address}
                scan_results.append(device_info)

                # Affiche les résultats dans la zone de texte
                result_str = f"Device: {hostname} - IP: {ip_address}\n"
                self.result_text.insert(tk.END, result_str)

        # Enregistre les résultats dans un fichier JSON
        self.save_results_to_json(scan_results)

        # Ajoute le fichier JSON au dépôt GitLab
        self.add_file_to_gitlab_repository()

    def save_results_to_json(self, results, output_file='scan_results.json'):
        with open(output_file, 'w') as json_file:
            json.dump(results, json_file, indent=2)
        print(f"Results saved to {output_file}")

        # Met à jour l'étiquette avec le message de confirmation
        self.confirmation_label.config(text="Scan results saved to JSON file.")

    def add_file_to_gitlab_repository(self):
        # Chemin vers votre répertoire local
        repo_path = r'C:\wamp64\www\Integration'

        # Chemin vers votre fichier JSON
        file_path = 'scan_results.json'

        # Nom de la branche
        branch_name = 'main'

        # Message de commit
        commit_message = "Ajout du fichier JSON"

        # Initialise le référentiel Git
        repo = Repo(repo_path)

        # Copie du fichier JSON dans le répertoire du référentiel
        shutil.copy(file_path, repo_path)

        # Ajoute le fichier JSON dans la zone de staging
        repo.git.add('.')

        # Effectue un commit avec un message
        repo.index.commit(commit_message)

        # Pousse les modifications vers le référentiel distant (GitLab)
        origin = repo.remote(name='origin')
        origin.push(branch_name)

        print("Tout est envoyé.")

def main():
    root = tk.Tk()
    app = NetworkScannerApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()
