<?php
session_start();
if(isset($_POST['logout'])) {
    // Détruire toutes les données de session
    session_destroy();

    // Rediriger vers la page de connexion ou une autre page après la déconnexion
    header('Location: login.php');
    exit;
}
?>
</html>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" media="screen" type="text/css" />
    <title>Nester</title>
    <style>
        body {
            background-color: #17596d;
            font-family: Arial, sans-serif;
            margin: 0;
            background-image: url('Image/photo2.jpg');
            background-size: cover;
            background-position: center;
        }

        #container {
            width: 80%;
            max-width: 800px;
            margin: 0 auto;
            margin-top: 10%;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
    <header>
        <form method="post" action="">
            <input type="submit" name="logout" value="Déconnexion">
        </form>
        <p class="connecter">Connecté en tant que : <?php $username = $_SESSION['username']; echo "{$username}"; ?></p>
        <h1>MSPR</h1>
    
    </header>
<body>

    <section class="main-content">
        <h2>Tableau nmap</h2>
        <p class="titre">Ici s'afficheront les données de nmap dans un tableau ( magnifique non ?) </p>
    </section>

    <?php

// Remplacez l'URL du fichier JSON sur GitLab par le vôtre
$json_url = 'https://gitlab.com/nicolaszekri/Integration/-/raw/main/scan_results.json?ref_type=heads';

// Récupérer les données JSON depuis GitLab
$json_data = file_get_contents($json_url);

// Décoder les données JSON
$data = json_decode($json_data, true);

// Vérifier si le décodage a réussi
if ($data !== null) {
    // Commencer la construction du tableau
    echo '<table border="1" style="border-collapse: collapse; width: 100%;">';
    
    // En-tête du tableau avec des couleurs de fond
    echo '<tr style="background-color: #f2f2f2;"><th>Device</th><th>IP</th></tr>';
    
    // Boucle à travers les données pour construire les lignes du tableau
    foreach ($data as $item) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($item['Device']) . '</td>';
        echo '<td>' . htmlspecialchars($item['IP']) . '</td>';
        echo '</tr>';
    }
    
    // Fin du tableau
    echo '</table>';
} else {
    // En cas d'échec du décodage JSON
    echo 'Erreur lors de la récupération des données depuis GitLab.';
}

?>


</body>
    <footer>
        <p>&copy; 2024 MSPR Tous droits pas trop reserves.</p>
    </footer>
</html>
