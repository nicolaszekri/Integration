<html>
    <head>
       <meta charset="utf-8">
        <!-- importer le fichier de style -->
        <link rel="stylesheet" href="style.css" media="screen" type="text/css" />
        <style>
        body {
            background-color: #17596d;
            font-family: Arial, sans-serif;
            margin: 0;
            background-image: url('Image/photo3.webp');
            background-size: cover;
            background-position: center;
        }

        #container {
            width: 80%;
            max-width: 800px;
            margin: 0 auto;
            margin-top: 10%;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }
    </style>
    </head>
    <body>
        <div id="container">
            <!-- zone de connexion -->
            
            <form action="verif.php" method="POST">
                <h1>Connexion</h1>
                <label><b>Identifiant</b></label>
                <input type="text" placeholder="Entrer l'identifiant" name="username" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>

                <input type="submit" id='submit' value='LOGIN' >
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
            </form>
        </div>
    </body>
</html>
